// Author : @omerjava -> https://gitlab.com/omerjava/

const ORIGIN = "http://localhost:8080/api";


// ************ create an employee -> listener, handler, api-call, render-data in html (display-data) **********************

// We added event listener to Button with id "submitBtn". When the button is clicked, handlerCreateData() function will be executed
document.getElementById("submitBtn").addEventListener("click", handlerCreateData);

// handlerCreateData() function will get inputs from HTML input fields and then will call employeeCreateApiCall
function handlerCreateData() {
  const nameInput = document.getElementById("nameCreate").value;
  const emailInput = document.getElementById("emailCreate").value;
  const positionInput = document.getElementById("positionCreate").value;

  employeeCreateApiCall(nameInput, emailInput, positionInput);

  nameInput.value = "";
  emailInput.value = "";
  positionInput.value = "";
}


// employeeCreateApiCall() function, by using fetch API, will send employee data to the backend in a request object
// and will receive a response object from backend which includes created employee in database
// and renderCreatedEmployee() function will be called to display data in HTML
const employeeCreateApiCall = (nameInput, emailInput, positionInput) => {
 
    fetch(`${ORIGIN}/employee`, {
    headers: {
      "Content-type": "application/json",
    },
    method: "POST",
    body: JSON.stringify({
      name: nameInput,
      email: emailInput,
      position: positionInput,
    }),
  })
    .then((response) => response.json())
    .then((data) => renderCreatedEmployee(data))
    .catch((err) => console.log(err.message));

};

// renderCreatedEmployee() function will create HTML elements and display data which come from backend/database
const renderCreatedEmployee = (data) => {
  document.getElementById("createEmployeeResult").innerHTML = `<h4>Id:</h4>
  <p>${data.id}</p>
  <h4>Name:</h4>
  <p>${data.name}</p>
  <h4>Email:</h4>
  <p>${data.email}</p>
  <h4>Position:</h4>
  <p>${data.position}</p>`;
};


// ************ get an employee by id -> listener, handler, api-call, render-data in html (display-data) **********************

document
  .getElementById("singleEmployeeBtn")
  .addEventListener("click", handlerFindEmployeeById);

function handlerFindEmployeeById() {
  const id = document.getElementById("singleEmployee").value;
  getEmployeeByIdApiCall(id);
}

const getEmployeeByIdApiCall = (employeeId) => {

  fetch(`${ORIGIN}/employee/${employeeId}`)
    .then((response) => response.json())
    .then((data) => renderEmployeeById(data))
    .catch(
      (error) =>
        (document.getElementById(
          "singleEmployeeResult"
        ).innerHTML = `We could not get employee due to ${error.message}`)
    );

};

const getEmployeeByIdApiCall2 = async (employeeId) => {
  try {
    const response = await fetch(`${ORIGIN}/employee/${employeeId}`);
    const data = await response.json();
    renderEmployeeById(data);
  } catch (error) {
    document.getElementById(
      "singleEmployeeResult"
    ).innerHTML = `We could not get employee due to ${error.message}`;
  }
};

const renderEmployeeById = (data) => {
  document.getElementById("singleEmployeeResult").innerHTML = `
  <h4>Id:</h4>
  <p>${data.id}</p>
  <h4>Name:</h4>
  <p>${data.name}</p>
  <h4>Email:</h4>
  <p>${data.email}</p>
  <h4>Position:</h4>
  <p>${data.position}</p>`;
};

// ************ update an employee -> listener, handler, api-call, render-data in html (display-data) **********************

document.getElementById("updateBtn").addEventListener("click", handlerUpdate);

function handlerUpdate() {
  const id = document.getElementById("idForUpdate").value;
  const name = document.getElementById("nameUpdate").value;
  const email = document.getElementById("emailUpdate").value;
  const position = document.getElementById("positionUpdate").value;

  employeeUpdateApiCall(id, name, email, position);

  id.value = "";
  name.value = "";
  email.value = "";
  position.value = "";
}

const employeeUpdateApiCall = (id, name, email, position) => {

  fetch(`${ORIGIN}/employee/${id}`, {
    method: "PUT",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      name: name,
      email: email,
      position: position,
    }),
  })
    .then((response) => response.json())
    .then((data) => renderUpdatedEmployee(data))
    .catch((error) => console.log(error.message));
};

const renderUpdatedEmployee = (data) => {
  document.getElementById("updateResult").innerHTML = `
  <h4>Id:</h4>
  <p>${data.id}</p>
  <h4>Name:</h4>
  <p>${data.name}</p>
  <h4>Email:</h4>
  <p>${data.email}</p>
  <h4>Position:</h4>
  <p>${data.position}</p>`;
};

// ************ delete an employee -> listener, handler, api-call **********************

document.getElementById("deleteBtn").addEventListener("click", handlerDelete);

function handlerDelete() {
  const id = document.getElementById("idForDelete").value;

  employeeDeleteApiCall(id);

  document.getElementById("idForDelete").value = "";
}

const employeeDeleteApiCall = (employeeId) => {

  fetch(`${ORIGIN}/employee/${employeeId}`, {
    method: "DELETE",
  })
    .then((res) =>
     res.ok
        ? (document.getElementById("deleteResult").innerHTML =
            "Deleted successfully!")
        : (document.getElementById("deleteResult").innerHTML =
            "Employee could not be deleted!")
    )
    .catch(
      (error) =>
        (document.getElementById(
          "deleteResult"
        ).innerHTML = `Employee could not be deleted due to ${error.message}`)
    );

};

// ************ get all employees -> listener, handler, api-call, render-data in html (display-data) **********************

document.getElementById("showAllBtn").addEventListener("click", handlerShowAll);

function handlerShowAll() {

  getAllEmployeesApiCall();

}

const getAllEmployeesApiCall = () => {

  fetch(`${ORIGIN}/employees`)
    .then((response) => response.json())
    .then((data) => renderShowAllEmployees(data))
    .catch((error) => console.log(error.message));
    
};

const renderShowAllEmployees = (data) => {
  const outerDiv = document.createElement("div");
  outerDiv.className = "outer";
  document.getElementById("showAllResult").innerHTML = "";

  for (let i = 0; i < data.length; i++) {
    const innerDiv = document.createElement("div");
    innerDiv.className = "inner";
    innerDiv.innerHTML = `<h4>Id:</h4>
                      <p>${data[i].id}</p>
                      <h4>Name:</h4>
                      <p>${data[i].name}</p>
                      <h4>Email:</h4>
                      <p>${data[i].email}</p>
                      <h4>Position:</h4>
                      <p>${data[i].position}</p>`;
    outerDiv.appendChild(innerDiv);
  }

  document.getElementById("showAllResult").appendChild(outerDiv);
};
